import React, { Component } from "react";
import Button from "./Components/button";
import Input from "./Components/input";
import Label from "./Components/label";


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      message: "",
      contact: "",
      list: []
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange = ({ target }) => {
    const { name, value } = target;
    this.setState({
      [name]: value
    });
  };

  onSubmit = () => {
    console.log("submit", "onSubmit Triggred");
    this.state.list.map(item => {
      console.log(item);
      
    });
    this.setState({
      ...this.state,
      list: this.state.list.concat({
        name: this.state.name,
        email: this.state.email,
        message: this.state.message,
        contact: this.state.contact
      })
    });
  };

  render() {
    return (
      <div className="App">
        <center>
          <header>
          <h1> React Form </h1>
        </header>
  
        <label name="Name" />
        <input
          type="text"
          name="name"
          placeholder="Enter the name"
          onChange={this.onChange}
          value={this.state.name}
          disabled={false}
        />
        <br />

        <label name="Email" />
        <input
          type="email"
          name="email"
          placeholder="Enter the email"
          onChange={this.onChange}
          value={this.state.email}
          disabled={false}
        />
        <br />

        <label name="Message" />
        <input
          type="textarea"
          name="message"
          placeholder="Enter the message"
          onChange={this.onChange}
          value={this.state.message}
          disabled={false}
        />
        <br />

        <label name="Contact" />
        <input
          type="number"
          name="contact"
          placeholder="Enter the contact"
          onChange={this.onChange}
          value={this.state.contact}
          disabled={false}
        />
        <br />

        <input
          type="submit"
          name="submit"
          onClick={this.onSubmit}
          id="button"
          disabled={this.state.name === "" ? true : false}
        />
        </center>
        <br />
        <br />

        {this.state.list.map((value, index) => (
          <div key={index}>
            <li> {value.name} </li>
            <li> {value.email} </li>
            <li> {value.message} </li>
            <li> {value.contact} </li>
            <hr />
          </div>
        ))}
      </div>
    );
  }
}

export default App;
